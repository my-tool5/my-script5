#!/usr/bin/env bash

    if [ -d ~/.local/bin ]; then
        echo "~/.local/bin detected, backing up..."
        # mkdir -p ~/.local/bin.old && mv ~/.local/bin/* ~/.local/bin.old/
        tar -czf ~/.local/bin.old."$(date +"%d%m%Y")".tgz ~/.local/bin
        cp -rf ./bin/* ~/.local/bin;
	# clear
    fi
    echo "Installing bin scripts..."
    mkdir -p ~/.local/bin && cp -r ./bin/* ~/.local/bin/;
	# clear
    SHELLNAME=$(echo "$SHELL" | grep -o '[^/]*$')
    case $SHELLNAME in
        bash)
            if [[ ":$PATH:" == *":$HOME/.local/bin:"* ]]; then
                echo "$HOME/.local/bin is already in your PATH(bash). Proceeding."
            else
                echo "Looks like $HOME/.local/bin is not on your PATH(bash), adding it now."
                echo "export PATH=\$PATH:\$HOME/.local/bin" >> "$HOME"/.bashrc
            fi
            ;;

        zsh)
            if [[ ":$PATH:" == *":$HOME/.local/bin:"* ]]; then
                echo "$HOME/.local/bin is already in your PATH(zsh). Proceeding."
            else
                echo "Looks like $HOME/.local/bin is not on your PATH(zsh), adding it now."
                echo "export PATH=\$PATH:\$HOME/.local/bin" >> "$HOME"/.zshrc
            fi
            ;;

        fish)
            echo "I see you use fish. shahab96 likes your choice."
            fish -c fish_add_path -P "$HOME"/.local/bin
            ;;

        *)
            echo "Please add: export PATH='\$PATH:$HOME/.local/bin' to your .bashrc or whatever shell you use."
            echo "If you know how to add stuff to shells other than bash, zsh and fish please help out here!"
    esac
